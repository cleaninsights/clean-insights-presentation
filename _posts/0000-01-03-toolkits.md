## Toolkits for Measurement

Clean Insights is available as a lightweight, minimal impact, freely licensed toolkit to include in your mobile app, desktop app, website or back-end service. 

--

The SDK code can be integrated into your application or service to measure specific events and interactions that you want to gain more insight on.

--

## Javascript SDK
For Progressive Web Apps, Browser Extensions, React apps, Websites and NodeJS Server Apps.

--

## Android SDK

Compatible with Java and Kotlin apps, on phones and other devices, across many Android SDKs.

--

## Apple SDK

Implemented in Swift compatible with Mobile, Desktop and other Apple platforms.

--

## Python SDK

For a variety of uses cases from command line tools, desktops apps and server infrastructure.



