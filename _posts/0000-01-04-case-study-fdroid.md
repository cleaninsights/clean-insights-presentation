## Case Study - F-Droid

A privacy-centric app store implements Clean Insights to understand what is popular
[more...](https://f-droid.org/en/2021/03/01/fdroid-metrics-and-clean-insights.html)

![F-Droid img](https://cleaninsights.org/img/posts/fdroid.jpg)

