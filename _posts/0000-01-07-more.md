### Thank you!

<backgroundimage>https://cleaninsights.org/img/header-bg.jpg</backgroundimage>
<backgroundimageopacity>0.2</backgroundimageopacity>

![logo](assets/images/cilogo128.png)

Learn more on our website at:
[https://cleaninsights.org](https://cleaninsights.org)


